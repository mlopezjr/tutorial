package test.automated.autotest.bankrate;

import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import test.automated.common.SetUp;
import test.automated.pageobjects.bankrate.CalculatorsPage;
import test.automated.pageobjects.bankrate.HomePage;
import test.automated.pageobjects.bankrate.MortgageCalculatorsPage;

public class CalculatorTest extends SetUp {
private HomePage homePage;
private CalculatorsPage calculatorsPage;
private MortgageCalculatorsPage mortgageCalculatorsPage;
	
	@BeforeMethod
	public void setUp() {
		homePage = new HomePage();
		calculatorsPage = new CalculatorsPage();
		mortgageCalculatorsPage = new MortgageCalculatorsPage();
	}

	@Test
	public void navigateMenus() throws InterruptedException {
		homePage.hoverMortgagesMenu();
		calculatorsPage.clickCalculatorsMenu();
		calculatorsPage.clickMortgageLoanCalc();
		longSleep();
	}
	
	/**
	 * 
	 * @throws TimeoutException if element isn't found
	 */
	@Test
	public void didCalculatorLoad() throws  TimeoutException {
		try {
			mortgageCalculatorsPage.verifyCalculatorLoads();
		} catch (TimeoutException e) {
			Assert.fail("The Calculator did not load correctly!");
		}
	}
	
	/**
	 * 
	 * @throws TimeoutException if element isn't found
	 */
	@Test
	public void printDefaultValues() {
		mortgageCalculatorsPage.getMortgageAmount();
		mortgageCalculatorsPage.getInterestRate();
		mortgageCalculatorsPage.getTermInYears();
	}
	
	/**
	 * 
	 * @throws InterruptedException for thread sleep
	 */
	@Test
	public void changeMortgageAmount() throws InterruptedException {
		mortgageCalculatorsPage.changeMortgageAmount();
	}
	
	/**
	 * 
	 * @throws TimeoutException if element isn't found
	 * @throws InterruptedException for thread sleep
	 */
	@Test
	public void changeTermInYears() {
		mortgageCalculatorsPage.changeTermInYears();
	}
	
	/**
	 * 
	 * @throws TimeoutException if element isn't found
	 * @throws InterruptedException for thread sleep
	 */
	@Test
	public void changeInterestRate() {
		mortgageCalculatorsPage.changeInterestRate();
	}
	
	/**
	 * 
	 * @throws InterruptedException for thread sleep
	 */
	@Test
	public void calculateMortgageRate() throws InterruptedException {
		mortgageCalculatorsPage.clickCalculateButton();
		mortgageCalculatorsPage.getMonthlyPayment();
		Assert.assertTrue(mortgageCalculatorsPage.getPaymentValue().equals("$3,615.62"));
		longSleep();
	}
	
	/**
	 * 
	 * @throws InterruptedException for thread sleep
	 */
	@Test
	public void resetCalculatorFields() throws InterruptedException {
		mortgageCalculatorsPage.resetMortgageAmount();
		mortgageCalculatorsPage.resetTerm();
		mortgageCalculatorsPage.resetInterestRate();
		longSleep();
	}
	
	/**
	 * 
	 * @throws InterruptedException for thread sleep
	 */
	@Test
	public void calculateDefaultMortgageRate() throws InterruptedException {
		mortgageCalculatorsPage.clickCalculateButton();
		mortgageCalculatorsPage.getMonthlyPayment();
		Assert.assertTrue(mortgageCalculatorsPage.getPaymentValue().equals("$1,529.99"));
		longSleep();
	}
	@AfterMethod
	public void tearDownPages() {
		homePage = null;
		calculatorsPage = null;
		mortgageCalculatorsPage = null;
	}
}

