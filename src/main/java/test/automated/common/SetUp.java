package test.automated.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;

public class SetUp {
	
	private static final int THREAD_SLEEP_VALUE = 2000;
	private static final int EXT_THREAD_SLEEP_VALUE = 8000;
	private static WebDriver driver;
	private static Actions builder;
	private static WebDriverWait wait;
	private static WebDriverWait longWait;
	private String browserType;
	private String webApp;

	/**
	 * Create getter for other Classes to access WebDriver
	 * @return driver
	 */	
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * Create getter for other Classes to access WebDriverWait
	 * @return wait
	 */
	public WebDriverWait getWait() {
		return wait;
	}
	
	/**
	 * Create getter for other Classes to access WebDriverWait
	 * @return longWait
	 */
	public WebDriverWait getLongWait() {
		return longWait;
	}

	/**
	 * Create getter for other Classes to access Actions
	 * @return builder
	 */
	public Actions getBuilder() {
		return builder;
	}
	
	/**
	 * Method to call get sleep
	 * @throws InterruptedException for thread sleep
	 */
	public void regSleep() throws InterruptedException {
		Thread.sleep(THREAD_SLEEP_VALUE);
	}
	
	/**
	 * Method to call get extended sleep
	 * @throws InterruptedException for extended sleep
	 */
	public void longSleep() throws InterruptedException {
		Thread.sleep(EXT_THREAD_SLEEP_VALUE);
	}
	
	/**
	 * create method to allow selection of browser based on parameter entered in
	 * TestNG xml file. set driver based on parameter and send to proper init
	 * browser method.
	 */

	private void setDriver() {
		switch (browserType) {
		case "ch":
			driver = initChromeDriver();
			break;
		case "ff":
			driver = initFirefoxDriver();
			break;
		}
	}

	// initiliaze Chrome driver and call Wait and Actions methods. Open web app.
	private WebDriver initChromeDriver() {
		driver = new ChromeDriver();
		initWait();
		initLongWait();
		initActions();
		driver.manage().window().maximize();
		driver.get(webApp);
		return driver;
	}

	/**
	 * initiliaze Firefox driver and call Wait and Actions methods. Open web
	 * app. Return driver so other classes can use.
	 */
	private WebDriver initFirefoxDriver() {
		driver = new FirefoxDriver();
		initWait();
		initLongWait();
		initActions();
		driver.manage().window().maximize();
		driver.get(webApp);
		return driver;
	}

	/**
	 * Create Actions methods for mouse and keyboard actions. Return builder so
	 * other classes can use.
	 */
	private Actions initActions() {
		builder = new Actions(driver);
		return builder;
	}

	/**
	 * Create WebDriverWait method to allow for pages to load before invoking
	 * actions. Return wait so other classes can use.
	 */
	private WebDriverWait initWait() {
		wait = new WebDriverWait(driver, 40, 100);
		return wait;
	}
	
	/**
	 * Create WebDriverWait method to allow for pages to load before invoking
	 * actions. Return wait so other classes can use.
	 */
	private WebDriverWait initLongWait() {
		longWait = new WebDriverWait(driver, 120, 100);
		return longWait;
	}
	
	/**
	 * Intializes base test setup
	 */
	@BeforeSuite
	protected void initializeTestBaseSetup() {
		try {
			initProperties();
			setDriver();
		} catch (Exception e) {
			Reporter.log("Error....." + e.getStackTrace());
		}
	}

	/**
	 * Close Webdriver
	 * @throws InterruptedException if driver fails to quit
	 */
	@AfterSuite
	protected void tearDown() throws InterruptedException {
		driver.quit();
	}
	
	/**
	 * Initializes java properties class.
	 * @throws IOException 
	 */
	protected void initProperties() throws IOException {
		Properties prop = new Properties();
		FileInputStream input = new FileInputStream("global.properties");
		prop.load(input);
		browserType = prop.getProperty("browserType");
		webApp = prop.getProperty("webApp");
	}
	
	/**
	 * Method to remove duplication in page object classes for waiting on visibility of element and then clicking on it
	 * @param element
	 */
	protected void clickOnVisibleElement(By element) {
		getWait().until(ExpectedConditions.visibilityOfElementLocated(element)).click();
	}
	
	/**
	 * Method to remove duplication in page object classes for waiting on visibility of element
	 * @param element
	 */
	protected void waitOnVisibleElement(By element) {
		getWait().until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
	/**
	 * Method to remove duplication in page object classes for waiting on visibility of element
	 * @param element
	 */
	protected void longWaitOnVisibleElement(By element) {
		getLongWait().until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
	
	/**
	 * Method to remove duplication in page object classes for waiting on visibility of element
	 * @param element
	 */
	protected void sendKeysToVisibleElement(By element, String text) {
		getWait().until(ExpectedConditions.visibilityOfElementLocated(element)).sendKeys(text);
	}
	
	/**
	 * Method to remove duplication in page object classes for waiting on visibility of element
	 * @param element
	 */
	protected void sendClearToVisibleElement(By element) {
		getWait().until(ExpectedConditions.visibilityOfElementLocated(element)).clear();
	} 
	
	/**
	 * Method to remove duplication in page object classes for checking if button is disabled
	 * @param element
	 */
	protected void verifyDisabledElement(By element, Boolean button) {
		button = getDriver().findElement(element).getAttribute("disabled") != null;
	}
	
	/**
	 * Method to remove duplication in page object classes for clicking on an element immediately
	 * @param element
	 */
	protected void immediateClickOnVisibleElement(By element) {
		getDriver().findElement(element).click();
	}
	
	/**
	 * Method to remove duplication in page object classes for sending keys to an element immediately
	 * @param element
	 */
	protected void immediateSendKeysToVisibleElement(By element, String text) {
		getDriver().findElement(element).sendKeys(text);
	}
	
	/**
	 * Method to remove duplication in page object classes for verifying an element isn't visible on the page
	 * @param element
	 */
	protected void verifyElementNotVisible(By element) {
		getWait().until(ExpectedConditions.not(ExpectedConditions.visibilityOfAllElementsLocatedBy(element)));
	}
}

