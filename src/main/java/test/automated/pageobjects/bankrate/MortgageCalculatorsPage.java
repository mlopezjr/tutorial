package test.automated.pageobjects.bankrate;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import test.automated.common.SetUp;

public class MortgageCalculatorsPage extends SetUp {
	private static final By CALCULATOR = By.id("KJECalculatorTable");

	private static final By MORTGAGE_AMOUNT = By.id("KJE-LOAN_AMOUNT");
	private static final By TERM = By.id("KJE-TERM");
	private static final By INTEREST_RATE = By.id("KJE-INTEREST_RATE");

	private static final By MORTGAGE_SLIDER = By.xpath("(//div[@class='KJEArrow'])[1]");
	private static final By POSITION_500 = By.xpath("//div[text()='$500k']");

	private static final By TERM_SLIDER = By.xpath("(//div[@class='KJEArrow'])[2]");

	private static final By INTEREST_SLIDER = By.xpath("(//div[@class='KJEArrow'])[4]");
	private static final By POSITION_8 = By.xpath("//div[text()='8%']");

	private static final By MONTHLY_PAYMENT = By.id("KJE-MONTHLY_PAYMENT");
	private static final By CALCULATE_BUTTON = By.id("KJECalculate");

	private static String mortgageAmount;
	private static String getTermInYears;
	private static String getInterestRate;
	private static String getMonthlyPayment;
	
	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage clickCalculateButton() {
		clickOnVisibleElement(CALCULATE_BUTTON);
		return this;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage verifyCalculatorLoads() {
		waitOnVisibleElement(CALCULATOR);
		return this;
	}

	/**
	 * 
	 * @return current page object
	 */
	public String getMortgageAmount() {
		mortgageAmount = getDriver().findElement(MORTGAGE_AMOUNT).getAttribute("value");
		Reporter.log("The default mortgage amount when page initially loaded was " + mortgageAmount);
		return mortgageAmount;
	}

	/**
	 * 
	 * @return current page object
	 */
	public String getTermInYears() {
		getTermInYears = getDriver().findElement(TERM).getAttribute("value");
		Reporter.log("The default term when page initially loaded was " + getTermInYears + " years");
		return getTermInYears;
	}

	/**
	 * 
	 * @return current page object
	 */
	public String getInterestRate() {
		getInterestRate = getDriver().findElement(INTEREST_RATE).getAttribute("value");
		Reporter.log("The default interest rate when page initially loaded was " + getInterestRate);
		return getInterestRate;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage getMonthlyPayment() {
		getMonthlyPayment = getDriver().findElement(MONTHLY_PAYMENT).getText();
		Reporter.log("The monthly payment for this loan is " + getMonthlyPayment);
		return this;
	}

	public String getPaymentValue() {
		return getMonthlyPayment;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage changeMortgageAmount() {
		WebElement slider = getWait().until(ExpectedConditions.elementToBeClickable(MORTGAGE_SLIDER));
		WebElement position = getWait().until(ExpectedConditions.elementToBeClickable(POSITION_500));
		getBuilder().dragAndDrop(slider, position).perform();
		return this;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage changeTermInYears() {
		WebElement slider = getWait().until(ExpectedConditions.elementToBeClickable(TERM_SLIDER));
		getBuilder().dragAndDropBy(slider, 100, 0).perform();
		return this;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage changeInterestRate() {
		WebElement slider = getWait().until(ExpectedConditions.elementToBeClickable(INTEREST_SLIDER));
		WebElement position = getWait().until(ExpectedConditions.elementToBeClickable(POSITION_8));
		getBuilder().dragAndDrop(slider, position).perform();
		return this;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage resetMortgageAmount() {
		WebElement field = getDriver().findElement(MORTGAGE_AMOUNT);
		field.clear();
		field.sendKeys(mortgageAmount);
		return this;
	}

	/**
	 * 
	 * @return current page object
	 */
	public MortgageCalculatorsPage resetTerm() {
		getDriver().findElement(TERM).sendKeys(getTermInYears);
		return this;
	}

	/**
	 * 
	 * @return current page object
	 * @throws TimeoutException if element isn't found
	 */
	public MortgageCalculatorsPage resetInterestRate() {
		WebElement field = getDriver().findElement(INTEREST_RATE);
		field.clear();
		field.sendKeys(getInterestRate);
		getBuilder().sendKeys(Keys.RETURN).perform();
		return this;
	}
}
