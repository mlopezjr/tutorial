package test.automated.pageobjects.bankrate;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

import test.automated.common.SetUp;

public class CalculatorsPage extends SetUp {
	private static final By MORTGAGE_RATE_CALC_LINK = By.xpath("//a[text()='Mortgage loan calculator']");
	private static final By CALCULATORS_MENU = By.xpath("(//a[text()='Calculators'])[2]");
	
	/**
	 * 
	 * @return current page object
	 */
	public CalculatorsPage clickMortgageLoanCalc() {
		clickOnVisibleElement(MORTGAGE_RATE_CALC_LINK);
		return this;
	}
	
	/**
	 * 
	 * @return current page objec
	 * @throws InterruptedException for thread sleep
	 */
	public MortgageCalculatorsPage clickCalculatorsMenu() throws InterruptedException {
		regSleep();
		clickOnVisibleElement(CALCULATORS_MENU);
		return new MortgageCalculatorsPage();
	}
}
