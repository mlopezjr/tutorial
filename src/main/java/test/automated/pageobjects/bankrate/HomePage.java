package test.automated.pageobjects.bankrate;

import test.automated.common.SetUp;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomePage extends SetUp {
	private static final By MORTGAGES_MENU = By.xpath("//a[text()='Mortgages']");

	/**
	 * Clicks Hover Menu
	 * @return current page object
	 * @throws TimeoutException if element isn't found
	 */
	public CalculatorsPage hoverMortgagesMenu() {
		WebElement menu = getWait().until(ExpectedConditions.elementToBeClickable(MORTGAGES_MENU));
		getBuilder().moveToElement(menu).build().perform();
		return new CalculatorsPage();
	}
}
